from django.urls import path
from templateapp import views
app_name='app'
urlpatterns = [
    path('about/',views.about,name='about'),
    path('contact/',views.contact,name='con'),
]
