from django.db import models


class feedback(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField(max_length=250)
    message = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)
    changed_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
