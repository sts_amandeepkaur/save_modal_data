from django.shortcuts import render
from templateapp.models import feedback

def home(request):
    return render(request,'home.html')

def about(request):
    return render(request,'about.html')

def contact(request):
    if request.method == 'POST':
        n = request.POST['name']
        e = request.POST['email']
        m = request.POST['message']

        all_data = feedback(name=n,email=e,message=m)
        all_data.save()
        return render(request,'contact.html',{'status':'Data Saved!!'})
    return render(request,'contact.html')