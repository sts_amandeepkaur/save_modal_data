from django.contrib import admin
from templateapp.models import feedback

admin.site.site_header='MySiTe'
admin.site.register(feedback)